import collections
import sys
from pathlib import Path
import fsa

import util

pwd = Path('./fsa').resolve()

def freq_table(seq):
    """
    in: ATGCATGCAT
    out:
        A : 3
        T : 3
        G : 2
        C : 2
    """
    ft = collections.Counter()
    for n in seq:
        ft[n] += 1
    return ft


def adj_freq_table(seq):
    """
    in: ATGCATGCAT
    out:
        AT : 3
        GC : 2
    """
    ft = collections.Counter()
    seq_len = len(seq)
    for i in range(0, seq_len, 2):
        cur_nt = seq[i]
        try:
            next_nt = seq[i+1]
        except:
            # failed to get next item
            next_nt = "!"
        ft[f"{cur_nt}-{next_nt}"] += 1
    return ft

def adj_freq_table_shared(seq):
    """
    in: ATGCATGCAT
    out:
        AT : 3
        TG : 2
        .
        .
    """
    ft = collections.Counter()
    seq_len = len(seq)
    for i in range(0, seq_len, 1):
        cur_nt = seq[i]
        try:
            next_nt = seq[i+1]
        except:
            next_nt = "!"
        ft[f"{cur_nt}-{next_nt}"] += 1
    return ft


for file in sorted(pwd.glob('*.fsa')):
    r = fsa.Record(file)
    print(
        '\n',
        file.name,
        f"{r.idx:12} {r.strain}:{r.chromosome or 'NA':6}",
        f"{len(r.sequence):7}",
        "\n" + ("-" * 80),
    )
    ft = freq_table(r.sequence)
    for n, f in list(ft.items()):
        ft[n+'/L'] = ft[n] / len(r.sequence)

    ft["A/G"] = ft['A'] / ft['G']
    util.print_sorted_kv(ft, max_rows=0)
    continue
    aft = adj_freq_table_shared(r.sequence)
    util.print_sorted_kv(aft, max_rows=0)
    #break