import sys
from pathlib import Path
import fsa

pwd = Path('./fsa').resolve()

hash_sizes = [int(n) for n in sys.argv[1].split(',')] if len(sys.argv) > 1 else [66]

for hash_size in hash_sizes:
    print('\n\nHash size:', hash_size)

    for file in sorted(pwd.glob('*.fsa')):
        r = fsa.Record(file)
        print(
            file.name,
            f"{r.idx:12} {r.strain}:{r.chromosome or 'NA':6}",
            f"{len(r.sequence):7}",
            r.color_hash(hash_size)
        )