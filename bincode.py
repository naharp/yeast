"""
Bincode converts fsa files to binary data where:
    A = 0, T = 1, G = 2, C = 3

NB: Using 0 as a significant value means we will suffer padding errors, but
good for now.

Code uses a sliding window of 4 BPs across a sequence and writes its merged
value to output file.
"""
import sys
from pathlib import Path
import fsa
pwd = Path('./fsa').resolve()
bindir = Path('./bincoded').resolve()
bindir.mkdir(exist_ok=True)

"""
AAAA = 0
AAAT = 1
CCCC = 255
"""

base = 'ATGC'
base_len = len(base)

def bincode_byte(s):
    v = 0
    for c in s:
        p = base.index(c)
        v = (v * base_len) + p
    return v

def write_bincode_sequence(s, out_file):
    fd = out_file.open('wb')
    for i in range(0, len(s), 4):
        slice = s[i:i+4]
        byte = bincode_byte(slice)
        fd.write(byte.to_bytes(1, 'little'))


for file in sorted(pwd.glob('*.fsa')):
    r = fsa.Record(file)
    print(
        file.name,
        f"{r.idx:12} {r.strain}:{r.chromosome or 'NA':6}",
        f"{len(r.sequence):7}"
    )
    out_file = bindir / file.name
    write_bincode_sequence(r.sequence, out_file)