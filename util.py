import json


def colored(r, g, b, text):
    return "\033[38;2;{};{};{}m{} \033[0;0m".format(r, g, b, text)

def pretty(obj):
    return json.dumps(obj, indent=1)

def print_sorted_kv(d:dict, max_rows=100):
    mv = max(d.values())
    i = 0
    for k,v in sorted(d.items(), key=lambda v:v[1], reverse=True):
        print(f"{k} = {v:8}   {'#' * int(v/mv * 60) }")
        i += 1
        if max_rows and i >= max_rows:
            break
