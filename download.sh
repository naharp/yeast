#!/bin/sh

mkdir -p fsa
cd fsa
wget -O files.xml 'http://sgd-archive.yeastgenome.org.s3-us-west-2.amazonaws.com/?delimiter=/&prefix=sequence/S288C_reference/chromosomes/fasta/'
for p in $(cat files.xml | tr \< \\n\\n | grep seque | cut -d \> -f2); do wget http://sgd-archive.yeastgenome.org/$p; done
