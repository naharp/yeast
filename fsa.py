import math
from collections import Counter
from pathlib import Path
from dataclasses import dataclass
import util


HASH_BLOCK_SIZE = 48
COLOR_BLOCK_SIZE = int(HASH_BLOCK_SIZE / 3)
MAX_COMPONENT = int('Z' * COLOR_BLOCK_SIZE, 36)

@dataclass
class Record:
    type: str
    idx: str
    organism: str
    strain: str
    moltype: str
    chromosome: str = None
    note: str = None
    sequence: str = None

    def __init__(self, fn):
        file = Path(fn)
        with file.open('r') as fd:
            self.type, self.idx, fields = fd.readline().split('|')
            for field in fields.split('['):
                if '=' not in field: continue
                key, val = field[:-2].split('=')
                if key == 'org': key = 'organism'
                setattr(self, key, val)
            self.sequence = ''.join((l.rstrip() for l in fd.readlines()))

    def hash(self, hash_size=HASH_BLOCK_SIZE):
        most_common = []
        block_size = math.ceil((len(self.sequence)) / hash_size)
        for i in range(0, len(self.sequence), block_size):
            block = self.sequence[i:i+block_size]
            most_common.append(Counter(block).most_common(1)[0][0])
        return ''.join(most_common)

    def color_hash(self, hash_size=HASH_BLOCK_SIZE):
        hash = self.hash(hash_size)

        color_block_size = max(int(hash_size / 3), 1)
        max_component = int('Z' * color_block_size, 36)

        r = int(hash[:color_block_size], 36)
        g = int(hash[color_block_size:color_block_size * 2], 36)
        b = int(hash[-color_block_size:], 36)

        r = int((r / max_component) * 255)
        g = int((g / max_component) * 255)
        b = int((b / max_component) * 255)

        return util.colored(r,g,b, hash)